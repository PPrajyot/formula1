# Databricks notebook source
dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/functions"

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

drivers_df = spark.read.format("delta").load(f"{processed_folder_path}/drivers").withColumnRenamed("name","driver_name").withColumnRenamed("number","driver_number").withColumnRenamed("nationality","driver_nationality")

# COMMAND ----------

constructors_df = spark.read.format("delta").load(f"{processed_folder_path}/constructor").withColumnRenamed("name","team").withColumnRenamed("constructoe_id","constructor_id").withColumnRenamed("constructoe_ref","constructor_ref")

# COMMAND ----------

circuits_df = spark.read.format("delta").load(f"{processed_folder_path}/circuits").withColumnRenamed("location","circuit_location")

# COMMAND ----------

races_df = spark.read.format("delta").load(f"{processed_folder_path}/races").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")

# COMMAND ----------

results_df = spark.read.format("delta").load(f"{processed_folder_path}/results").filter(f"data_source = '{value_file_date}'").withColumnRenamed("time","race_time").withColumnRenamed("constructorId","constructor_id").withColumnRenamed("race_id","result_race_id").withColumnRenamed("data_source","result_data_source")

# COMMAND ----------

# MAGIC %md
# MAGIC ######join

# COMMAND ----------

race_circ_df = races_df.join(circuits_df,races_df.circuit_id == circuits_df.circuit_id,"inner").select ("race_id","race_year","race_name","race_date","circuit_location")

# COMMAND ----------

race_results_df = results_df.join(race_circ_df,results_df.result_race_id == race_circ_df.race_id).join(drivers_df, 
                                   results_df.driver_id == drivers_df.driver_id).join(constructors_df,results_df.constructor_id == constructors_df.constructor_id)

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

final_df = race_results_df.select("race_year","race_name","race_date","circuit_location","driver_name","driver_number","driver_nationality","team","grid","fastest_lap","race_time","points","position","race_id","result_data_source").withColumn("created_date",current_timestamp()).withColumnRenamed("result_data_source","data_source")

# COMMAND ----------

# overwrite_partition(final_df,"f1_presentation","race_results","race_id")

# COMMAND ----------

merge_condition = "tgt.driver_name = src.driver_name AND tgt.race_id = src.race_id"

merge_delta_data(final_df,'f1_presentation','race_results',presentation_folder_path,merge_condition,'race_id')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- drop table f1_presentation.race_results

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,count(1)
# MAGIC from f1_presentation.race_results
# MAGIC group by race_id
# MAGIC order by race_id desc
