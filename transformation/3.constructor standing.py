# Databricks notebook source
dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

# MAGIC %run "../Includes/functions"

# COMMAND ----------

race_result_df = spark.read.format("delta").load(f"{presentation_folder_path}/race_results")

# COMMAND ----------

race_year_list = col_to_list(race_result_df,'race_year')

# COMMAND ----------

from pyspark.sql.functions import sum,count,when,col

# COMMAND ----------

race_result_df = spark.read.format("delta").load(f"{presentation_folder_path}/race_results").filter(col("race_year").isin(race_year_list))

# COMMAND ----------

constructor_standing_df = race_result_df.groupBy("race_year","team").agg(sum("points").alias("total_points"),count(when(col("position") == 1 , True)).alias("wins"))

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import desc,rank,asc

# COMMAND ----------

constructor_rank_spec = Window.partitionBy("race_year").orderBy(desc("total_points"),desc("wins"))
final_df = constructor_standing_df.withColumn("rank",rank().over(constructor_rank_spec))

# COMMAND ----------

# overwrite_partition(final_df,"f1_presentation","contructor_standing","race_year")

# COMMAND ----------

merge_condition = "tgt.team = src.team AND tgt.race_year = src.race_year"

merge_delta_data(final_df,"f1_presentation","contructor_standing",presentation_folder_path,merge_condition,'race_year')

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_year,count(1)
# MAGIC from f1_presentation.contructor_standing
# MAGIC group by race_year
# MAGIC order by race_year desc

# COMMAND ----------

# MAGIC %sql
# MAGIC -- drop table f1_presentation.contructor_standing

# COMMAND ----------


