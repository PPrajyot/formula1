# Databricks notebook source
dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

# MAGIC %run "../Includes/functions"    

# COMMAND ----------

# MAGIC %md
# MAGIC #####find the race year for which the data is needed to be reprocessed

# COMMAND ----------

race_result_list = spark.read.format("delta").load(f"{presentation_folder_path}/race_results").filter(f"data_source = '{value_file_date}'").select ("race_year").distinct().collect()

# COMMAND ----------

race_year_list = []
for race_result in race_result_list:
    race_year_list.append(race_result.race_year)

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

race_result_df = spark.read.format("delta").load(f"{presentation_folder_path}/race_results").filter(col("race_year").isin(race_year_list))

# COMMAND ----------

from pyspark.sql.functions import sum,count,when,col

# COMMAND ----------

driver_standing_df = race_result_df.groupBy("race_year","driver_name","driver_nationality").agg(sum("points").alias("total_points"),count(when(col("position") == 1 , True)).alias("wins"))

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import desc,rank,asc

# COMMAND ----------

driver_rank_spec = Window.partitionBy("race_year").orderBy(desc("total_points"),desc("wins"))
final_df = driver_standing_df.withColumn("rank",rank().over(driver_rank_spec))

# COMMAND ----------

# overwrite_partition(final_df,"f1_presentation","driver_standing","race_year")

# COMMAND ----------

merge_condition = "tgt.driver_name = src.driver_name AND tgt.race_year = src.race_year"

merge_delta_data(final_df,"f1_presentation","driver_standing",presentation_folder_path,merge_condition,'race_year')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- drop table f1_presentation.driver_standing

# COMMAND ----------


