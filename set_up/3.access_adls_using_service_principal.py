# Databricks notebook source
# MAGIC %md
# MAGIC ### Access Azure data lake using service principal.
# MAGIC 1. Register AD Application/Service Principal.
# MAGIC 2. Generate a secret/password for the application.
# MAGIC 3. Set spark config with App/Client id,Directory/ Tenant id & Secret
# MAGIC 4. Assign role 'Storage blob data contributor' to the Data lake
# MAGIC

# COMMAND ----------

client_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'clientid-app-formula1')
tenant_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'tenantid-app-formula1')
client_secret = dbutils.secrets.get(scope = 'formula1-scope',key = 'client-secret-app-formula1')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula1pract.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.formula1pract.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.formula1pract.dfs.core.windows.net", client_id)
spark.conf.set("fs.azure.account.oauth2.client.secret.formula1pract.dfs.core.windows.net", client_secret)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.formula1pract.dfs.core.windows.net", f"https://login.microsoftonline.com/{tenant_id}/oauth2/token") 

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1pract.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1pract.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


