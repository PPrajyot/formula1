# Databricks notebook source
# MAGIC %md
# MAGIC ####Explore DBFS root
# MAGIC 1.List all folders in DBFS root.
# MAGIC 2.interact with DBFS file browser.
# MAGIC 3.upload file to dbfs root.

# COMMAND ----------

display(dbutils.fs.ls('/'))

# COMMAND ----------

display(dbutils.fs.ls('/FileStore'))

# COMMAND ----------

display(spark.read.csv('/FileStore/002_circuits.csv'))

# COMMAND ----------


