# Databricks notebook source
# MAGIC %md
# MAGIC ### Access Azure data lake using cluster scope credentials.
# MAGIC 1. set spark config fs.azure.account.key in the cluster
# MAGIC 2. list files from emo container.
# MAGIC 3. read the data from circuits.
# MAGIC

# COMMAND ----------

we need to give this configuration in the cluster as a key value pair.
#spark.conf.set("fs.azure.account.key.formula1pract.dfs.core.windows.net","nTRWvNjC3rFy5cJ2R3EdjNJZBE6kBFHtP8frEIeCNW4G8sXJEnabx/XSFE3uYxwCWaAuDn9a2iB0+AStuHB/Vg==")

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1pract.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1pract.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


