# Databricks notebook source
# MAGIC %md
# MAGIC ### Access Azure data lake using SAS token. 
# MAGIC 1. set spark config for SAS token.
# MAGIC 2. list files from emo container.
# MAGIC 3. read the data from circuits.
# MAGIC

# COMMAND ----------

SAS_for_formula1 = dbutils.secrets.get(scope = 'formula1-scope',key = 'formula1-demo-SAS')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula1pract.dfs.core.windows.net", "SAS")
spark.conf.set("fs.azure.sas.token.provider.type.formula1pract.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.formula1pract.dfs.core.windows.net", SAS_for_formula1 )

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1pract.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1pract.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


