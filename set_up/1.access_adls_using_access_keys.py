# Databricks notebook source
# MAGIC %md
# MAGIC ### Access Azure data lake using access key 
# MAGIC 1. set spark config fs.azure.account.key
# MAGIC 2. list files from emo container.
# MAGIC 3. read the data from circuits.
# MAGIC

# COMMAND ----------

formula1_acc_key = dbutils.secrets.get(scope = 'formula1-scope',key = 'formula1pract-account-key')

# COMMAND ----------

spark.conf.set("fs.azure.account.key.formula1pract.dfs.core.windows.net",formula1_acc_key)

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1pract.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1pract.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


