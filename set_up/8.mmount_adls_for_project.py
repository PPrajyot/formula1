# Databricks notebook source
# MAGIC %md
# MAGIC ### Mount Azure data lake for project.

# COMMAND ----------

def mount_adls(storage_account_name,cont_name):
    #secrets
    client_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'clientid-app-formula1')
    tenant_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'tenantid-app-formula1')
    client_secret = dbutils.secrets.get(scope = 'formula1-scope',key = 'client-secret-app-formula1')

    #configs
    configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}
    
    if any(mount.mountPoint == f"/mnt/{storage_account_name}/{cont_name}" for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(f"/mnt/{storage_account_name}/{cont_name}")

    #mount
    dbutils.fs.mount(
        source = f"abfss://{cont_name}@{storage_account_name}.dfs.core.windows.net/",
        mount_point = f"/mnt/{storage_account_name}/{cont_name}",
        extra_configs = configs)
    
    display(dbutils.fs.mounts())

# COMMAND ----------

mount_adls('formula1pract','raw')

# COMMAND ----------

mount_adls('formula1pract','process')

# COMMAND ----------

mount_adls('formula1pract','presentation')

# COMMAND ----------

client_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'clientid-app-formula1')
tenant_id = dbutils.secrets.get(scope = 'formula1-scope',key = 'tenantid-app-formula1')
client_secret = dbutils.secrets.get(scope = 'formula1-scope',key = 'client-secret-app-formula1')

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}

# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://demo@formula1pract.dfs.core.windows.net/",
  mount_point = "/mnt/formula1pract/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/formula1pract/demo"))

# COMMAND ----------

display(spark.read.csv("/mnt/formula1pract/demo/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

dbutils.fs.unmount('/mnt/formula1pract/demo')

# COMMAND ----------


