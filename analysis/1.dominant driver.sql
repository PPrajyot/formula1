-- Databricks notebook source
select driver_name,count(race_year) as total_races,sum(calculated_points) as total_points,avg(calculated_points) as avg_points
 from f1_presentation.calculated_race_results 
group by driver_name
having count(race_year) >= 50 
order by avg_points desc

-- COMMAND ----------


