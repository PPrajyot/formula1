# Databricks notebook source
from pyspark.sql.functions import current_timestamp

# COMMAND ----------

def add_ingestion_date (input_df):
    output_df = input_df.withColumn("ingestion_date",current_timestamp())
    return output_df 

# COMMAND ----------

def rearrange_part_col(ip_df,partition_column):
    column_list = []
    for column_name in ip_df.schema.names:
        if column_name != partition_column:
            column_list.append(column_name)
    column_list.append(partition_column)

    print(column_list)

    op_df = ip_df.select(column_list)
    return op_df

# COMMAND ----------

def overwrite_partition(input_df,db_name,table_name,partition_column):
  output_df = rearrange_part_col(input_df,partition_column)
  
  spark.conf.set("spark.sql.sources.partitionOverwriteMode","dynamic")
  if (spark._jsparkSession.catalog().tableExists(f"{db_name}.{table_name}")):
    output_df.write.mode("overwrite").insertInto(f"{db_name}.{table_name}")
  else:
    output_df.write.mode("overwrite").partitionBy(partition_column).format("parquet").saveAsTable(f"{db_name}.{table_name}") 


# COMMAND ----------

def col_to_list(input_df,column_name):
    df_row_list = input_df.select(column_name).distinct().collect()

    col_value_list = [row[column_name] for row in df_row_list]
    return col_value_list

# COMMAND ----------

def merge_delta_data(ip_df,db_name,table_name,folder_path,merge_condition,partition_column):
    spark.conf.set("spark.databricks.optimizer.dynamicPartitionPruning","true")

    from delta.tables import DeltaTable 
    if (spark._jsparkSession.catalog().tableExists(f"{db_name}.{table_name}")):
        deltaTable = DeltaTable.forPath(spark,f"{folder_path}/{table_name}")
        deltaTable.alias("tgt").merge(ip_df.alias("src"),merge_condition).whenMatchedUpdateAll().whenNotMatchedInsertAll().execute()
    else:
        ip_df.write.mode("overwrite").partitionBy(partition_column).format("delta").saveAsTable(f"{db_name}.{table_name}") 

# COMMAND ----------


