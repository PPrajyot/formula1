-- Databricks notebook source
CREATE DATABASE IF NOT EXISTS f1_raw;

-- COMMAND ----------

show databases

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####table for circuits.csv

-- COMMAND ----------

drop table if exists f1_raw.circuits;
create table if not exists f1_raw.circuits(
circuitId INT,
circuitRef STRING,
name STRING,
location STRING,
country STRING,
lat double,
lng double,
alt INT,
url STRING
)
using csv
options (path "/mnt/formula1pract/raw/circuits.csv")

-- COMMAND ----------

select * from f1_raw.circuits;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####table for races

-- COMMAND ----------

drop table if exists f1_raw.races;
create table if not exists f1_raw.races(
raceId int,
year int,
round int,
cuircitId int,
name string,
date date,
time string,
url string
)
using csv
options (path "/mnt/formula1pract/raw/races.csv")

-- COMMAND ----------

select * from f1_raw.races;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####constructors json file

-- COMMAND ----------

drop table if exists f1_raw.constructors;
create table if not exists f1_raw.constructors(
constructorId int,
constructorRef string,
name string,
nationality string,
url string
)
using json
options (path "/mnt/formula1pract/raw/constructors.json")

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####drivers table

-- COMMAND ----------

drop table if exists f1_raw.drivers;
create table if not exists f1_raw.drivers(
driverId int,
driverRef string,
number int,
code string,
name struct <forename : string,surname : string>,
forename string,
surname string,
bob date,
nationality string,
url string
)
using json
options (path "/mnt/formula1pract/raw/drivers.json")

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####results json

-- COMMAND ----------

drop table if exists f1_raw.results;
create table if not exists f1_raw.results(
constructorId int,
driverId int,
fastestLap int,
fastestLapSpeed float,
fastestLapTime string,
grid int,
laps int,
milliseconds int,
number int,
points float,
position int,
positionOrder int,
positionText string,
raceId int,
rank int,
resultId int,
statusId string,
time string
)
using json
options (path "/mnt/formula1pract/raw/results.json")

-- COMMAND ----------

select * from f1_raw.results;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####pitstops table

-- COMMAND ----------

drop table if exists f1_raw.pitstops;
create table if not exists f1_raw.pitstops(
driverId int,
duration string,
lap int,
milliseconds int,
raceId int,
stop int,
time string
)
using json
options (path "/mnt/formula1pract/raw/pit_stops.json",multiLine true)

-- COMMAND ----------

select * from f1_raw.pitstops;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####tables from folders/list of files

-- COMMAND ----------

-- MAGIC %md 
-- MAGIC #####lap times table

-- COMMAND ----------

drop table if exists f1_raw.laptime;
create table if not exists f1_raw.laptime(
raceId int,
driverId int,
lap int,
position int,
time string,
milliseconds int
)
using csv
options (path "/mnt/formula1pract/raw/lap_times")

-- COMMAND ----------

select * from  f1_raw.laptime

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####qualifying table

-- COMMAND ----------

drop table if exists f1_raw.qualifying;
create table if not exists f1_raw.qualifying(
qualifyId int,
raceId int,
driverId int,
constructorId int,
number int,
position string,
q1 string,
q2 string,
q3 string
)
using json
options (path "/mnt/formula1pract/raw/qualifying",multiLine true)

-- COMMAND ----------

 select * from f1_raw.qualifying

-- COMMAND ----------


