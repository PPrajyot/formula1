# Databricks notebook source
# MAGIC %md
# MAGIC #####Ingest constructor.jason file

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

constructors_schema = "constructorId INT , constructorRef STRING, name STRING, nationality STRING, url STRING"

# COMMAND ----------

constructors_df = spark.read.schema(constructors_schema).json(f"dbfs:{raw_folder_path}/{value_file_date}/constructors.json")

# COMMAND ----------

# MAGIC %md
# MAGIC #####drop a column
# MAGIC

# COMMAND ----------

constructor_drop_df = constructors_df.drop('url')  
 #also constructor_drop_df = constructors_df.drop(constructor_df['url']) 

# COMMAND ----------

# MAGIC %md
# MAGIC #####rename col

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

constructor_final_df = constructor_drop_df.withColumnRenamed("constructorId","constructor_id").withColumnRenamed("constructorRef","constructor_ref").withColumn("ingestion_date",current_timestamp()).withColumn("data_source",lit(value_file_date))

# COMMAND ----------

# MAGIC %md
# MAGIC #####write to parquet

# COMMAND ----------

constructor_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.constructor")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------


