# Databricks notebook source
# MAGIC %run "../Includes/Configurations"   

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType,FloatType

# COMMAND ----------

results_schema = StructType ([  StructField("constructorId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("fastestLap",IntegerType()),\
                                StructField("fastestLapSpeed",FloatType()),\
                                StructField("fastestLapTime",StringType()),\
                                StructField("grid",IntegerType()),\
                                StructField("laps",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("number",IntegerType()),\
                                StructField("points",FloatType()),\
                                StructField("position",IntegerType()),\
                                StructField("positionOrder",IntegerType()),\
                                StructField("positionText",StringType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("rank",IntegerType()),\
                                StructField("resultId",IntegerType()),\
                                StructField("statusId",StringType()),\
                                StructField("time",StringType())
                                
                ])

# COMMAND ----------

results_df = spark.read.schema(results_schema).json(f"dbfs:{raw_folder_path}/{value_file_date}/results.json")

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

results_withcol_df = results_df.withColumnRenamed("resultID","result_id")\
                               .withColumnRenamed("raceId","race_id")\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumnRenamed("constructorId","constructor_id")\
                                .withColumnRenamed("positionText","position_text")\
                                .withColumnRenamed("positionOrder","position_order")\
                                .withColumnRenamed("fastestLap","fastest_lap")\
                                .withColumnRenamed("fastestLapTime","fastest_lap_time")\
                                .withColumnRenamed("fastestLapSpeed","fastest_lap_speed")\
                                .withColumn("ingestion_time",current_timestamp())\
                                .withColumn("data_source",lit(value_file_date))
                        

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

results_final_df = results_withcol_df.drop(col("statusId"))

# COMMAND ----------

results_final_df = results_final_df.dropDuplicates(['race_id','driver_id'])

# COMMAND ----------

# MAGIC %md
# MAGIC #####method 1

# COMMAND ----------

# for race_id_list in results_final_df.select("race_id").distinct().collect():
#     if (spark._jsparkSession.catalog().tableExists("f1_processed.results")):
#         spark.sql(f"alter table f1_processed.result drop if exists partition (race_id = race_id_list.race_id)")
    

# COMMAND ----------

# results_final_df.write.mode("append").partitionBy("race_id").format("parquet").saveAsTable("f1_processed.result")

# COMMAND ----------

# MAGIC %md
# MAGIC #####method 2

# COMMAND ----------

# MAGIC %run "../Includes/functions"

# COMMAND ----------

# overwrite_partition(results_final_df,"f1_processed","results","race_id")

# COMMAND ----------

merge_condition = "tgt.result_id = src.result_id AND tgt.race_id = src.race_id"

merge_delta_data(results_final_df,'f1_processed','results',processed_folder_path,merge_condition,'race_id')

# COMMAND ----------

results_final_df = results_final_df.select("constructor_id",
"driver_id",
"fastest_lap",
"fastest_lap_speed",
"fastest_lap_time",
"grid",
"laps",
"milliseconds",
"number",
"points",
"position",
"position_order",
"position_text",
"rank",
"result_id",
"time",
"ingestion_time",
"data_source","race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,count(1)
# MAGIC from f1_processed.results
# MAGIC group by race_id
# MAGIC order by race_id desc

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,driver_id,count(*) from f1_processed.results
# MAGIC group by race_id,driver_id
# MAGIC having count(*)>1
# MAGIC

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------

# MAGIC %sql
# MAGIC drop table f1_processed.results

# COMMAND ----------


