# Databricks notebook source
# MAGIC %md
# MAGIC ####ingest qualifying folder

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

# MAGIC %run "../Includes/functions"

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType

# COMMAND ----------

qualifying_schema = StructType ([ StructField("qualifyId",IntegerType()),  
                                StructField("raceId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("constructorId",IntegerType()),\
                                StructField("number",IntegerType()),\
                                StructField("position",StringType()),\
                                StructField("q1",StringType()),\
                                StructField("q2",StringType()),\
                                StructField("q3",StringType())
                              
])

# COMMAND ----------


qualifying_df = spark.read.schema(qualifying_schema).option("multiLine",True).json(f"dbfs:{raw_folder_path}/{value_file_date}/qualifying")

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

qualifying_final_df = qualifying_df.withColumnRenamed("qualifyId","qualifying_id")\
                    .withColumnRenamed("raceId","race_id").withColumnRenamed("driverId","driver_id")\
                    .withColumnRenamed("constructorId","constructor_id")\
                    .withColumn("ingestion_date",current_timestamp()).withColumn("data_source",lit(value_file_date))

# COMMAND ----------

# overwrite_partition(qualifying_final_df,"f1_processed","qualifying","race_id")

# COMMAND ----------

merge_condition = "tgt.qualifying_id = src.qualifying_id AND tgt.race_id = src.race_id"

merge_delta_data(qualifying_final_df,"f1_processed","qualifying",processed_folder_path,merge_condition,'race_id')

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,count(1)
# MAGIC from f1_processed.qualifying
# MAGIC group by race_id
# MAGIC order by race_id desc

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------

# MAGIC %sql
# MAGIC drop table f1_processed.qualifying

# COMMAND ----------


