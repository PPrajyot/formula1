# Databricks notebook source
# MAGIC %md
# MAGIC ####Ingest circuits.csv file

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %md
# MAGIC #####change the schema

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

# MAGIC %run "../Includes/functions"

# COMMAND ----------

raw_folder_path

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,IntegerType,StringType,DoubleType

# COMMAND ----------

circuits_schema = StructType(fields=[ StructField("circuitId",IntegerType(),False),
                                      StructField("circuitRef",StringType(),True),
                                      StructField("name",StringType(),True),
                                      StructField("location",StringType(),True),
                                      StructField("country",StringType(),True),
                                      StructField("lat",DoubleType(),True),
                                      StructField("lng",DoubleType(),True),
                                      StructField("alt",IntegerType(),True),
                                      StructField("url",StringType(),True)

])

# COMMAND ----------

circuits_df = spark.read.option("header",True).schema(circuits_schema).csv(f"dbfs:{raw_folder_path}/{value_file_date}/circuits.csv")

# COMMAND ----------

# MAGIC %md
# MAGIC ##### select required columns

# COMMAND ----------

circuits_select_col = circuits_df.select("circuitId","circuitRef","name","location","country","lat","lng","alt")

# COMMAND ----------

# MAGIC %md
# MAGIC #####rename the column 

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

circuit_rename_df = circuits_select_col.withColumnRenamed("circuitId","circuit_id")\
.withColumnRenamed("circuitRef","circuit_ref").withColumnRenamed("lat","latitude").withColumnRenamed("lng","longitude").withColumnRenamed("alt","altitude").withColumn("data_source",lit(value_file_date))


# COMMAND ----------

display(circuit_rename_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ##### ingest a column

# COMMAND ----------

circuits_final_df = add_ingestion_date(circuit_rename_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #####write data to datalake as parquet

# COMMAND ----------

circuits_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.circuits")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------


