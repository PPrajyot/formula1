# Databricks notebook source
# MAGIC %md
# MAGIC ####ingest laptime folder

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

# MAGIC %run "../Includes/functions"
# MAGIC

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType

# COMMAND ----------

laptime_schema = StructType ([ StructField("raceId",IntegerType()),  
                                 StructField("driverId",IntegerType()),\
                                StructField("lap",IntegerType()),\
                                StructField("position",IntegerType()),\
                                StructField("time",StringType()),\
                                StructField("milliseconds",StringType())
                              
])

# COMMAND ----------

laptimes_df = spark.read.schema(laptime_schema).csv(f"{raw_folder_path}/{value_file_date}/lap_times")

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

laptimes_final_df = laptimes_df.withColumnRenamed("driverId","driver_id").withColumnRenamed("raceId","race_id")\
    .withColumn("ingestion_date",lit(current_timestamp())).withColumn("data_source",lit(value_file_date))

# COMMAND ----------

# overwrite_partition(laptimes_final_df,"f1_processed","laptimes","race_id")

# COMMAND ----------

merge_condition = "tgt.race_id = src.race_id AND tgt.driver_id = src.driver_id AND tgt.lap = src.lap AND tgt.race_id = src.race_id"

merge_delta_data(laptimes_final_df,'f1_processed','laptimes',processed_folder_path,merge_condition,'race_id')

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,count(1)
# MAGIC from f1_processed.laptimes
# MAGIC group by race_id
# MAGIC order by race_id desc
# MAGIC
# MAGIC

# COMMAND ----------


