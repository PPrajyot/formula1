# Databricks notebook source
# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,IntegerType,StringType,DateType


# COMMAND ----------

name_schema = StructType(fields = [StructField("forename" ,StringType(),True),
                                   StructField("surname" ,StringType(),True)
                                    ])

# COMMAND ----------

driver_schema = StructType(fields=[ StructField("driverId", IntegerType(),False),
                                   StructField("driverRef" ,StringType(),True),
                                   StructField("number" ,IntegerType(),True),
                                   StructField("code" ,StringType(),True),
                                   StructField("name" ,name_schema),
                                   StructField("bob", DateType(),True),
                                   StructField("nationality" ,StringType(),True),
                                   StructField("url" ,StringType(),True)
                                   ])

# COMMAND ----------

drivers_df = spark.read.schema(driver_schema).json(f"dbfs:{raw_folder_path}/{value_file_date}/drivers.json")

# COMMAND ----------

from pyspark.sql.functions import col,concat,current_timestamp,lit

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

driver_withcol_df = drivers_df.withColumnRenamed("driverId","driver_id").withColumnRenamed("driverRef","driver_ref") \
                     .withColumn("ingestion_date", current_timestamp()).withColumn("name",concat(col('name.forename'),lit(' '),col('name.surname'))).withColumn("data_source",lit(value_file_date))

# COMMAND ----------

drivers_final_df = driver_withcol_df.drop(col("url"))

# COMMAND ----------

drivers_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.drivers")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------


