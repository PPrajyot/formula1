# Databricks notebook source
dbutils.widgets.text("pass_file_date","")
value_file_date = dbutils.widgets.get("pass_file_date") 

# COMMAND ----------

# MAGIC %run "../Includes/Configurations"

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,IntegerType,StringType,DateType

# COMMAND ----------

races_schema = StructType(fields=[ StructField("raceId",IntegerType(),False),
                                      StructField("year",IntegerType(),True),
                                      StructField("round",IntegerType(),True),
                                      StructField("cuircitId",IntegerType(),True),
                                      StructField("name",StringType(),True),
                                      StructField("date",DateType(),True),
                                      StructField("time",StringType(),True),
                                      StructField("url",StringType(),True) 
                                    ])

# COMMAND ----------

races_df = spark.read.option("header",True).schema(races_schema).csv(f"dbfs:{raw_folder_path}/{value_file_date}/races.csv")

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,to_timestamp,concat,col,lit

# COMMAND ----------

race_timestamp_df = races_df.withColumn("ingestion_date",current_timestamp())\
.withColumn("race_timestamp",to_timestamp(concat(col('date'),lit(' '),col('time')),'yyyy-MM-dd HH:mm:ss')).withColumn("data_source",lit(value_file_date))

# COMMAND ----------

races_selected_df = race_timestamp_df.select(col('raceId').alias('race_id'),col('year').alias('race_year'),'round',col('cuircitId').alias('circuit_id'),'name','ingestion_date','race_timestamp')

# COMMAND ----------

races_selected_df.write.mode('overwrite').partitionBy('race_year').format("delta").saveAsTable("f1_processed.races")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------


